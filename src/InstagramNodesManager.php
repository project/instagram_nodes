<?php

namespace Drupal\instagram_nodes;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\FileRepositoryInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\ClientException;

/**
 * Service description.
 */
class InstagramNodesManager {
  use StringTranslationTrait;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The file handler.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file.repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs an InstagramNodesManager object.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   * @param \Drupal\file\FileRepositoryInterface $repository
   *   The file.repository service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   */
  public function __construct(
    MailManagerInterface $mail_manager,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
    FileSystemInterface $file_system,
    FileRepositoryInterface $repository,
    LoggerChannelFactoryInterface $logger) {
    $this->mailManager = $mail_manager;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->fileSystem = $file_system;
    $this->fileRepository = $repository;
    $this->logger = $logger;
  }

  /**
   * Gets the posts and if they're new creates a node for them.
   */
  public function updatePosts() {
    $posts = $this->getPosts();
    if (isset($posts['data'])) {
      foreach ($posts['data'] as $post) {
        if ($this->postIsNew($post['id'])) {
          $this->createNode($post);
        }
      }
      $this->purgeNodes();
      $this->logger->get('instagram_nodes')->notice($this->t('Instagram posts imported'));
    }
  }

  /**
   * Gets the posts from the Instagram API.
   *
   * @return array
   *   An array of accounts with corresponding posts.
   */
  public function getPosts() {
    $config = $this->configFactory->get('instagram_nodes.config');
    $url = 'https://graph.instagram.com/me/media?fields=id,caption,timestamp,media_url,media_type,permalink,thumbnail_url,children{media_url}&access_token=';
    $token = $config->get('token');
    $limit = $config->get('limit');
    try {
      $response = $this->httpClient->get($url . $token . '&limit=' . $limit);
      $data = (string) $response->getBody();
      $decodedData = Json::decode($data);
    }
    catch (ClientException $e) {
      $this->logger->get('instagram_nodes')->error($this->t('Error importing instagram posts'));
      if ($config->get('send_emails') && !empty($config->get('contact_emails'))) {
        $params['email'] = $config->get('contact_emails');
        $this->sendEmail($params);
      }
    }
    return $decodedData ?? [];
  }

  /**
   * Creates a node from the post.
   *
   * @param array $post
   *   Array with the post info.
   */
  private function createNode(array $post) {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $captionText = preg_replace('/[^(\x20-\x7F)]*/', '', $post['caption']);
    $caption = [
      'value' => !empty($captionText) ? $captionText : '',
      'format' => 'full_html',
    ];
    $title = !empty($captionText) ? mb_substr($captionText, 0, 25) : $post['id'];
    $timestamp = strtotime($post['timestamp']);
    $imageUrl = $post['media_url'];
    $instagramImage = [];
    // For videos use the thumbnail.
    if (strpos($imageUrl, 'https://video') !== FALSE || (isset($post['media_type']) && $post['media_type'] == 'VIDEO')) {
      $imageUrl = $post['thumbnail_url'];
    }
    if ((isset($post['media_type']) && $post['media_type'] == 'CAROUSEL_ALBUM')) {
      foreach ($post['children']['data'] as $child) {
        $imageFid = $this->createImage($child['id'], $child['media_url']);
        $instagramImage[] = [
          'target_id' => $imageFid,
          'alt' => 'instagram post ' . $title,
        ];
      }
    }
    else {
      $imageFid = $this->createImage($post['id'], $imageUrl);
      $instagramImage = [
        'target_id' => $imageFid,
        'alt' => 'instagram post ' . $title,
      ];
    }
    $node = $nodeStorage->create([
      'type' => 'instagram_post',
      'title' => $title,
      'field_instagram_caption' => $caption,
      'field_instagram_id' => $post['id'],
      'field_instagram_timestamp' => $timestamp,
      'field_instagram_url' => $post['permalink'],
      'field_instagram_image' => $instagramImage,
    ]);
    $node->save();
  }

  /**
   * Creates a image file.
   *
   * @param mixed $id
   *   The image id.
   * @param string $imageUrl
   *   The URL of the image that you want to download and save locally.
   *
   * @return int
   *   The file id.
   */
  private function createImage($id, $imageUrl) {
    $imageData = file_get_contents($imageUrl);
    $dir = "public://instagram";
    if ($imageData && $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY)) {
      $file = $this->fileRepository->writeData($imageData, $dir . '/' . $id . '.jpeg', FileSystemInterface::EXISTS_REPLACE);
      if ($file) {
        $imageFid = $file->get('fid')->getString();
      }
    }
    return $imageFid;
  }

  /**
   * Purges the nodes to the limit (deletes the older).
   */
  private function purgeNodes() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $config = $this->configFactory->get('instagram_nodes.config');
    $limit = $config->get('limit');
    $query = $nodeStorage->getQuery()
      ->condition('type', 'instagram_post');
    $query->sort('field_instagram_timestamp', 'DESC')->accessCheck(FALSE);
    $results = $query->execute();
    $total = count($results);
    if ($total > $limit) {
      $toDelete = array_slice($results, $limit);
      foreach ($toDelete as $nid) {
        $node = $nodeStorage->load($nid);
        $node->delete();
      }
    }
  }

  /**
   * Verifies if post is new.
   *
   * @param int $id
   *   The post id.
   *
   * @return bool
   *   TRUE if the post is new FALSE otherwise.
   */
  public function postIsNew($id) {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $query = $nodeStorage->getQuery()
      ->condition('type', 'instagram_post')
      ->condition('field_instagram_id', $id)
      ->accessCheck(FALSE);
    $result = $query->execute();
    return empty($result);
  }

  /**
   * Sends emails on fail.
   *
   * @param array $params
   *   Mail parameters.
   */
  private function sendEmail(array $params) {
    $module = 'instagram_nodes';
    $key = 'expired_token';
    $body = $this->t("There was an error updating the instagram posts. Please check if your access token has expired.");
    $params['body'] = $body;
    $params['subject'] = "Error updating instagram Posts";
    $this->mailManager->mail($module, $key, $params['email'], 'en', $params, NULL, TRUE);
  }

}
