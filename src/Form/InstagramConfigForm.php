<?php

namespace Drupal\instagram_nodes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\instagram_nodes\InstagramNodesManager;

/**
 * Configuration for social media.
 */
class InstagramConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The instagram_nodes.helper service.
   *
   * @var \Drupal\instagram_nodes\InstagramNodesManager
   */
  protected $instagramNodesManager;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    InstagramNodesManager $instagram_nodes_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->instagramNodesManager = $instagram_nodes_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('instagram_nodes.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = ['instagram_nodes'];
    return Cache::mergeTags(parent::getCacheTags(), $tags);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'instagram_nodes.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_nodes_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('instagram_nodes.config');
    $form['token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Acess Token'),
      '#default_value' => $config->get('token') ? $config->get('token') : '',
    ];
    $form['contact_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact emails'),
      '#description' => $this->t('Emails to contact when tokens expire. Please separate multiple emails with ","'),
      '#default_value' => $config->get('contact_emails'),
    ];
    $form['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum posts to keep'),
      '#default_value' => $config->get('limit') ? $config->get('limit') : 15,
    ];
    $form['cron_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Cron execution Interval'),
      '#description' => $this->t('Minimum time in hours between imports.'),
      '#min' => 1,
      '#default_value' => $config->get('cron_interval'),
    ];
    $form['send_emails'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send email on expiration'),
      '#default_value' => $config->get('send_emails'),
      '#description' => $this->t("If checked emails will be sent on token expiration, it's advised to check this in production environments"),
    ];
    $form['update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update Posts on Save'),
      '#default_value' => FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('instagram_nodes.config');
    $config->set('contact_emails', $values['contact_emails'])
      ->set('send_emails', $values['send_emails'])
      ->set('limit', $values['limit'])
      ->set('token', $values['token'])
      ->set('cron_interval', $values['cron_interval']);
    $config->save();
    if ($values['update']) {
      $this->instagramNodesManager->updatePosts();
    }
    Cache::invalidateTags(['instagram_nodes']);
  }

}
