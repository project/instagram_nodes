<?php

/**
 * @file
 * Primary hooks for instagram_nodes module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function instagram_nodes_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.instagram_nodes':
      $readme = __DIR__ . '/README.md';
      $text = file_get_contents($readme);
      $output = '';

      // If the Markdown module is installed, use it to render the README.
      if ($text && \Drupal::moduleHandler()->moduleExists('markdown') === TRUE) {
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        $output = $filter->process($text, 'en');
      }
      // Else the Markdown module is not installed output the README as text.
      elseif ($text) {
        $output = '<pre>' . $text . '</pre>';
      }
      return $output;
  }
}

/**
 * Implements hook_cron().
 */
function instagram_nodes_cron() {
  $config = \Drupal::service('config.factory')->get('instagram_nodes.config');
  $stateService = \Drupal::service('state');
  $cronInterval = $config->get('cron_interval') ?: 1;
  $lastExecutionTime = $stateService->get('instagram_nodes_cron', 0);
  $currentTime = time();
  $timeDifference = $currentTime - $lastExecutionTime;
  $interval = $cronInterval * 3600;
  if ($timeDifference >= $interval || $lastExecutionTime == 0) {
    $instagramNodesManager = \Drupal::service('instagram_nodes.manager');
    $instagramNodesManager->updatePosts();
    $stateService->set('instagram_nodes_cron', $currentTime);
  }
}

/**
 * Implements hook_mail().
 */
function instagram_nodes_mail($key, &$message, $params) {
  switch ($key) {
    case 'expired_token':
      $message['subject'] = $params['subject'];
      $message['body'][] = $params['body'];
      $message['headers']['Content-Type'] = 'text/html;';
      break;
  }
}
