# Instagram Nodes

This module creates a content type where imported instagram posts are stored.
The posts are automatically imported during cron or manually in a configuration form.
The fact that the post are stored in Drupal reduces the calls to the api if you
have a block or a page showing them, wich some times can end up in the requests being blocked.
The posts can then be used in a view or custom block like any other content type in Drupal.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/instagram_nodes).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/instagram_nodes).

## Table of contents

- Requirements
- Installation
- Features
- Configuration
- Maintainers

## Requirements

This module requires that you have an api key to import the posts.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Features

Creation of instagram_nodes content type.
Import of posts on cron or configuration form.
Email sending on token expiration.

## Configuration

The module provides a configuration form where you can configure:

- Acess Token: The instagram Acess Token.
- Contact emails: The emails where a message should be sent when the token expires.
- Maximum posts to keep: Maximum post to keep, when we have more than the defined
  the older posts are purged.
- Cron execution Interval: Minumum interval in between crons where posts will be imported.
- Send email on expiration: If you wish to receive an email when the token expires.
- Update Posts on Save: If selected posts will be imported on save

## Maintainers

- Paulo Calado - [kallado](https://www.drupal.org/u/kallado)
- João Mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)

This project has been sponsored by:

- Visit: [Javali](https://www.javali.pt) for more information
